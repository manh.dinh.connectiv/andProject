# I. Kiểm tra các function chưa tối ưu

## function 1

IN File  : backend/app/Http/Controllers/Api/ProfileController.php

![Screenshot](function1.png)

Trong function này đang sử dụng validate trực tiếp bên trong luôn.

Trong controler chỉ để xữ lí logic. Như vây sẽ làm phình controller. Và khó bảo trì.

Giải pháp : sử dung form request để  tách phân validate riêng.

## function 2

IN File  : backend/app/Http/Controllers/Api/BadgeController.php

![Screenshot](function2.png)

Khi truy xuất dữ liêu thì hạn chế  dùng findOrFail bởi vì nêu khi tri xuất mà không tim thấy dữ liệu đúng yêu câu sẽ đưa ra lỗi 500 và các dong code bên dưới sẽ k đc thực thi nưa.

Giải pháp :

Có thể dùng first() nếu truy vân không có kết quả nó sẽ tự trả về null

Để tìm record trên database với ID cho trước có thể sữ dung:

- Route Model Binding

    https://laravel.com/docs/8.x/routing#route-model-binding

## function 3

IN File : backend/app/Http/Controllers/Api/PostController.php

![Screenshot](function3.png)

Hàm này nay check role của user nhưng viết rất dài dòng. làm phình code không đáng

Giai pháp:

Sử dụng middleware khai báo trong hàm construct

    $this->middleware('role:Administrator')->only('changeScopesPost');

## function 4

IN File : backend/app/Entities/Post.php

![Screenshot](function4.png)

Không phải lỗi nhưng code thừa .

Thay vì dùng vòng lặp từng phần từ trỏ về id rôi push vào array rổng thì mình dùng pluck('id') cũng đưa ra kết quả tương tự ,

    $list = $listFolower->get()->toArray();
            $listArrayUser = [];
            for ($j=0; $j < sizeOf($list); $j++) {
                array_push($listArrayUser, $list[$j]->id);
            }

            return $listArrayUser;

Hàm nay cũng tương tư như thế

![Screenshot](function4+.png)

## function 5

In File : backend/app/Http/Controllers/Api/PrVideoController.php

![Screenshot](function5.png)

Nêu hàm trong controlller không đùng resource route . THì để tìm record trên database với ID cho trước có thể sữ dung:

- Route Model Binding

    https://laravel.com/docs/8.x/routing#route-model-binding

có rất nhiêu function không dùng.

## Vân đề  6

em nghĩ Không viết các hàm truy vân trong thư mục Model.

    - backend/app/Entities/Post.php
    - backend/app/Entities/User.php

Viết trong file Repository chăng han ^^

## function 7

In File : backend/app/Http/Controllers/Api/Company/CompanyController.php

![Screenshot](function7.png)

Function thự hiên chức năng lưu chỉn sữa trong db nhưng mà không bắt validate . nêu người dùng nhập quá ki tự cho phép hoặc sai kiêu dữ liệu trong db sẽ web sẽ bị lôi,

Giải pháp: thêm form validate để check data đâu vào hợp lê

## Vấn đề 8

![Screenshot](function8.png)

có rất nhiêu hàm  không comment chức năng, param, return của nó.
hầu như controller trong project nào cũng có.

## Vân đề 9

em thấy bên FE , code style css trong component luôn cái nào it thì con dê dọc chứ nhiều thì em lướt mãi k thấy đáy .
nên em nghĩ là tach biệt css riêng ra dê bảo trì và quan lí code.
